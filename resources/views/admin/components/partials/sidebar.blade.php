<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
                <a href="index.html">Dlh</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">Dlh</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="nav-item dropdown active">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('admin.auth.dashboard')}}">Dashboard</a></li>
                </ul>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Master</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('admin.auth.jurusan.index')}}">Jurusan</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.VisiMisi.index')}}">Visi Misi</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.Struktur.index')}}">Struktur</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.karyawan.index')}}">karyawan</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.kategori.index')}}">kategori</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.berita.index')}}">Berita</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.profile.index')}}">Profile</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.kepsek.index')}}">Kepsek</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.sarana.index')}}">Sarana</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.galery.index')}}">galery</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.pengumuman.index')}}">Pengumuman</a></li>
{{--                    <li><a class="nav-link" href="{{route('admin.auth.news.index')}}">Berita</a></li>--}}
{{--                    <li><a class="nav-link" href="{{route('admin.auth.ppid.index')}}">PPID</a></li>--}}
{{--                    <li><a class="nav-link" href="{{route('admin.auth.newGalleries.index')}}">Gallery</a></li>--}}
{{--                    <li><a class="nav-link" href="{{route('admin.auth.agenda.index')}}">Agenda</a></li>--}}
                    {{--                        <li><a class="nav-link" href="{{route('admin.auth.carousel.index')}}">Carousel</a></li>--}}
                    {{--                        <li><a class="nav-link" href="{{route('admin.auth.pariwara.index')}}">Pariwara</a></li>--}}
                    {{--                        <li><a class="nav-link" href="{{route('admin.auth.relatedWebsites.index')}}">website terkait</a></li>--}}
                    {{--                        <li><a class="nav-link" href="{{route('admin.auth.report.index')}}">laporan</a></li>--}}
                    {{--                        <li><a class="nav-link" href="{{route('admin.auth.videoProfile.index')}}">Video Profile</a></li>--}}

                </ul>
            </li>
    </aside>
</div>
