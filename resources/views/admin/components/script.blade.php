
<!-- General JS Scripts -->
<script src="{{asset('asset-stisla/js/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('asset-stisla/js/popper.min.js')}}"></script>
<script src="{{asset('asset-stisla/js/bootstrap.min.js')}}" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="{{asset('asset-stisla/js/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('asset-stisla/js/moment.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{asset('asset-stisla/js/stisla.js')}}"></script>

<!-- Template JS File -->
<script src="{{asset('asset-stisla/js/scripts.js')}}"></script>
<script src="{{asset('asset-stisla/js/custom.js')}}"></script>

<!-- Page Specific JS File -->
{{--<script src="{{asset('asset-stisla/js//page/index.js')}}"></script>--}}

<!-- JS Libraies -->
<script src="{{asset('asset-stisla/js/datatables.min.js')}}"></script>
<script src="{{asset('asset-stisla/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('asset-stisla/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('asset-stisla/js/jquery-ui.min.js')}}"></script>
{{--<script src="../assets/js/page/index-0.js"></script>--}}
{{--<script src="{{asset('asset-stisla/js/page/index-0.js')}}"></script>--}}

<!-- Page Specific JS File -->
<script src="{{asset('asset-stisla/js/modules-datatables.js')}}"></script>

<script src="https://demo.getstisla.com/assets/modules/summernote/summernote-bs4.js"></script>
