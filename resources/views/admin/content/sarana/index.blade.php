@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">karyawan</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
{{--                                @if($data == null)--}}
                                    <a href="{{route('admin.auth.sarana.create')}}" class="btn btn-success text-white">create</a>
{{--                                @endif--}}
                                <br><br>
                                @include('admin.components.partials.message')
                                <div class="table-responsive">
                                    <table class=" table table-striped" id="item">
                                        <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Image</th>
                                            <th>Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $sarana)
                                            <tr>
                                                <td>{{$sarana->name}}</td>
                                                <td><img src="{{asset($sarana->img)}}" width="50"></td>

                                                <td>
                                                    <a href="{{route('admin.auth.sarana.edit',$sarana->id)}}" class="btn btn-primary text-white">Edit</a>
                                                    <a href="{{route('admin.auth.saranaDestroy',$sarana->id)}}" onclick="return confirm(`Are you sure?`)" class="btn btn-danger text-white">Delete</a>
                                                </td>
                                            </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                });
            });
        </script>

    @endpush
@endsection
