@extends('admin.app')

@section('content')


<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Form New Galleries</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{($data!=null) ? route('admin.auth.newGalleries.update',$data->id) : route('admin.auth.newGalleries.store')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if($data!=null)
                                        @method('put')
                                    @endif
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{($data!=null) ? $data->name : ''}}">
                                            @error('name')
                                                <div class="alert alert-danger">{{ $message  }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Desc</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="desc" id="name" class="form-control @error('desc') is-invalid @enderror" value="{{($data!=null) ? $data->desc : ''}}">
                                            @error('desc')
                                                <div class="alert alert-danger">{{ $message  }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="image[]" id="image" multiple="true">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($data!=null)
                <div class="section-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Image New Galleries</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{route('admin.auth.newGalleries.selectDelete')}}" method="post" >
                                        @csrf
                                        <div class="form-group">
                                                <label class="form-label">Images</label><br>
                                                <div class="row gutters-sm">
                                                    @forelse($galleries as $gallery)
                                                        <div class="col-6 col-sm-4">
                                                            <label class="imagecheck mb-4">
                                                                <input name="images[]" id="thumbnail" type="checkbox" value="{{$gallery->id}}" class="imagecheck-input checkBoxClass" />
                                                                <figure class="imagecheck-figure">
                                                                    <img src="{{asset($gallery->img)}}" alt="}" class="imagecheck-image">
                                                                </figure>
                                                            </label>
                                                        </div>
                                                    @empty
                                                        <a>Kamu tidak memiliki galeri</a>
                                                    @endforelse                                                </div>
                                            <div class="form-group row mb-4">
                                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                                <div class="col-sm-12 col-md-7">
                                                    <input type="checkbox" id="checkAll">Check all</input>

                                                </div>
                                            </div>
                                            <div class="form-group row mb-4">
                                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                                <div class="col-sm-12 col-md-7">
                                                    <button class="btn btn-danger">Hapus</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
    </div>
    <script>
        $(function (e){
            $("#checkAll").click(function (){
                $(".checkBoxClass").prop('checked',$(this).prop('checked'));
            });
        });

    </script>
@endsection
