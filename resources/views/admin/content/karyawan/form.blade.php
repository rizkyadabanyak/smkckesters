@extends('admin.app')

@section('content')


<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Form karyawan</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{($data!=null) ? route('admin.auth.karyawan.update',$data->id) : route('admin.auth.karyawan.index')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if($data!=null)
                                        @method('put')
                                    @endif
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{($data!=null) ? $data->name : ''}}">
                                            @error('name')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="image" id="image">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Mapel</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="mapel" id="mapel" class="form-control @error('mapel') is-invalid @enderror" value="{{($data!=null) ? $data->mapel : ''}}">
                                            @error('mapel')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Mapel</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="form-control" id="exampleFormControlSelect1" name="type">
                                                @if($data!=null)
                                                    @if($data->type == '0')
                                                        <option value="0" selected>Tenaga Kependidikan</option>
                                                    @else
                                                        <option value="1" selected>Guru</option>
                                                    @endif
                                                @endif
                                                <option value="0">Tenaga Kependidikan</option>
                                                <option value="1">Guru</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>
@endsection
