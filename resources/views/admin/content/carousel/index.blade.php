@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Carousel</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <a href="{{route('admin.auth.carousel.create')}}" class="btn btn-success text-white">create</a>

                                <br><br>
                                @include('admin.components.partials.message')
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="item">
                                            <thead>
                                            <tr>
                                                <th>User</th>
                                                <th>Name</th>
                                                <th>Desc</th>
                                                <th>Img</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.carousel.index')}}",
                    },
                    columns: [
                        {
                            data: 'user',
                            name: 'user'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'desc',
                            name: 'desc'
                        },
                        {
                            data: 'img',
                            name: 'img'
                        },
                        {
                            data: 'active_flag',
                            name: 'active_flag'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },

                    ]
                });
            });
        </script>

    @endpush
@endsection
