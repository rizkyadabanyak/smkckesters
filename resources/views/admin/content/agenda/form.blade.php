@extends('admin.app')

@section('content')


<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Form Agenda</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{($data!=null) ? route('admin.auth.agenda.update',$data->id) : route('admin.auth.agenda.store')}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    @if($data!=null)
                                        @method('put')
                                    @endif

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{($data!=null) ? $data->name : ''}}">
                                            @error('name')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="image" id="image">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Desc</label>
                                        <div class="col-sm-12 col-md-7">
                                            <textarea id="summernote" name="desc">{!! ($data!=null) ? $data->desc : '' !!}</textarea>
                                            @error('desc')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Location</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="location" id="location" class="form-control @error('location') is-invalid @enderror" value="{{($data!=null) ? $data->location : ''}}">
                                            @error('location')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Date Start </label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="date" name="date_start" id="date_start" class="form-control @error('date_start') is-invalid @enderror" value="{{($data!=null) ? date('Y-m-d', strtotime($data->date_start)) :''}}">
                                            @error('date_start')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">End Start</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="date" name="date_end" id="date_end" class="form-control @error('date_end') is-invalid @enderror" value="{{($data!=null) ? date('Y-m-d', strtotime($data->date_end)) :''}}">
                                            @error('date_end')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Time</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="time" name="time" class="form-control @error('time') is-invalid @enderror" value="{{($data!=null) ? $data->time : ''}}">
                                            @error('time')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>
@endsection
