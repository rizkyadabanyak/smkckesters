@extends('admin.app')

@section('content')


<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Form Jurusan</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{($data!=null) ? route('admin.auth.kepsek.update',$data->id) : route('admin.auth.kepsek.index')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if($data!=null)
                                        @method('put')
                                    @endif


                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">dsc</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="dsc" id="dsc" class="form-control @error('dsc') is-invalid @enderror" value="{{($data!=null) ? $data->dsc : ''}}">
                                            @error('dsc')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="image" id="image">
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

        $(document).ready(function() {
            $('#summernotea').summernote();
        });

    </script>
@endsection
