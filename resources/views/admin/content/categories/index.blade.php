@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Categories</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{route('admin.auth.kategori.create')}}" class="btn btn-success text-white">create</a>
                                <br><br>
                                @include('admin.components.partials.message')
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $ketegori)
                                        <tr>
                                            <td>{{$ketegori->name}}</td>
                                            <td>
                                                <a href="{{route('admin.auth.kategori.edit',$ketegori->id)}}" class="btn btn-primary text-white">Edit</a>
                                                <a href="{{route('admin.auth.kategoriDestroy',$ketegori->id)}}" onclick="return confirm(`Are you sure?`)" class="btn btn-danger text-white">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')


    @endpush

@endsection
