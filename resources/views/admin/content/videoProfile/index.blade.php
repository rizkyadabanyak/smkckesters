@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>About Us</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Profile</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if($data == null)
                                    <a href="{{route('admin.auth.videoProfile.create')}}" class="btn btn-success text-white">create</a>
                                @endif
                                <br><br>
                                @include('admin.components.partials.message')
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>vidio</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            @if($data!=null)
                                                <td>{{$data->name}}</td>
                                                <td>{{$data->src}}</td>
                                                <td>
                                                    <a  href="{{route('admin.auth.videoProfile.edit',$data->id)}}" class="btn btn-info" ><i class="fa fa-trash"></i> Edit</a>
                                                    <a  href="{{route('admin.auth.videoProfileDestroy',$data->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</a>
                                                </td>
                                            @else
                                                <td>No have data</td>
                                            @endif
                                        </tr>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
