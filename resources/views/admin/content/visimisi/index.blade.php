@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Jurusan</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
{{--                                @if($data == null)--}}
{{--                                    <a href="{{route('admin.auth.VisiMisi.create')}}" class="btn btn-success text-white">create</a>--}}
{{--                                @endif--}}
                                <br><br>
                                @include('admin.components.partials.message')
                                <div class="table-responsive">
                                    <table class=" table table-striped" id="item">
                                        <thead>
                                        <tr>
                                            <th>Visi</th>
                                            <th>Misi</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <td>{{$data->visi}}</td>
                                            <td>{{$data->misi}}</td>
                                            <td>
                                                <a href="{{route('admin.auth.VisiMisi.edit',$data->id)}}" class="btn btn-primary text-white">Edit</a>
{{--                                                <a href="{{route('admin.auth.VisiMisiDestroy',$data->id)}}" onclick="return confirm(`Are you sure?`)" class="btn btn-danger text-white">Delete</a>--}}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                });
            });
        </script>

    @endpush
@endsection
