@extends('landing.app')

@section('content')
    @include('landing.components.style-search')

    <style>
        .card-horizontal {
            display: flex !important;
            flex: 1 1 auto !important;
        }
        * {
            box-sizing: border-box;
        }

        /* Add a gray background color with some padding */
        /* Header/Blog Title */
        .header {
            font-size: 40px;
            text-align: center;
            background: white;
        }

        /* Create two unequal columns that floats next to each other */
        /* Left column */
        .leftcolumn {
            float: left;
            width: 75%;
        }

        /* Right column */
        .rightcolumn {
            float: left;
            width: 25%;
            padding-left: 20px;
        }

        /* Fake image */
        .fakeimg {
            background-color: #aaa;
            width: 100%;
            padding: 20px;
        }

        /* Add a card effect for articles */
        .card {
            border: 0px;
            background-color: var(--color-bg);
            padding: 20px;
            margin-top: 20px;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/

        }/* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Footer */
        .footer {
            padding: 20px;
            text-align: center;
            background: #ddd;
            margin-top: 20px;
        }

        /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 800px) {
            .leftcolumn, .rightcolumn {
                width: 100%;
                padding: 0;
            }
        }
        .shadow-card{
            box-shadow: rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px;
        }
        .zoom {
            -webkit-transition: all 0.35s ease-in-out;
            -moz-transition: all 0.35s ease-in-out;
            transition: all 0.35s ease-in-out;
            cursor: -webkit-zoom-in;
            cursor: -moz-zoom-in;
            cursor: zoom-in;
        }
        .zoom:hover,
        .zoom:active,
        .zoom:focus {
            /**adjust scale to desired size,
            add browser prefixes**/
            -ms-transform: scale(4);
            -moz-transform: scale(4);
            -webkit-transform: scale(4);
            -o-transform: scale(4);
            transform: scale(4);
            position:relative;
            z-index:100;
        }
    </style>
    <div class="header">
        <div style="height:300px;background-image: url('https://accessnsite.com/wp-content/uploads/2020/05/page-heading-background-contact-us.jpg');box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></div>
    </div>

    <div class="container py-5" style="max-width: 1200px;padding: 0px">

        <section class="wrapper">
            <div class="row">
                <div class="leftcolumn">
                    <div class="card shadow-card" style="">
                        <h2>{{$data->name}}</h2>
                        <h5>{{$data->created_at}}</h5>
                        <img src="{{asset($data->img)}}" class="img-fluid" alt="Responsive image">

                        <p style="color: var(--color-font);">{!! $data->desc !!}</p>

                    </div>

                </div>
                <div class="rightcolumn">
                    <div class="card shadow-card">
                        <a style="border-bottom: 6px #5ca863 solid;font-size: 20px;" class="text-orange">Cari Berita</a>
                        <form action="{{route('searchAction')}}" method="post" class="mb-5">
                            @csrf
                            <div class="d-flex justify-content-center ">
                                <div class="search"> <input type="text" class="typeahead search-input" placeholder="Search..." name="search"> <button class="search-icon"> <i class="fa fa-search"></i> </button> </div>
                            </div>
                        </form>
                    </div>
                    <div class="card shadow-card">
                        <a style="border-bottom: 6px #5ca863 solid;font-size: 20px;" class="text-orange">Berita Terbaru</a>

                        <div class="row">
                            @forelse($news as $new)
                                <div class="col-12">
                                    <div class="card" style="border: 0px;padding-top: 2px;padding-bottom: 2px">
                                        <div class="card-horizontal">
                                            <div class="img-square-wrapper">
                                                <img class="img-card" src="{{asset($new->img)}}" alt="Card image cap" style="width: 100px;height: 100px;margin-top: 20px">
                                            </div>
                                            <div class="card-body">
                                                <a href="{{route('detail',$new->slug)}}" style="color: black">
                                                    <p class="card-title" style="font-family: 'Segoe UI';font-size: 10px">{{\Str::limit($new->name, 70, $end='...') }}</p>
                                                </a>
                                                <p class="card-text" style="font-family: 'Segoe UI';color: #5ca863;font-size: 10px">{{$new->kategori->name}}</p>
                                                <p class="card-text" style="font-family: 'Segoe UI';color: var(--color-scnd);font-size: 10px">{!! \Str::limit($new->desc_banner, 30, $end='...') !!}</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @empty
                                <h5 class="mt-5">belum ada berita</h5>
                            @endforelse
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>

@endsection
