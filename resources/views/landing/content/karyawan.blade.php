@extends('landing.app')

@section('content')
    @include('landing.components.style-search')

    <style>
        .card-horizontal {
            display: flex !important;
            flex: 1 1 auto !important;
        }
        * {
            box-sizing: border-box;
        }

        /* Add a gray background color with some padding */
        /* Header/Blog Title */
        .header {
            font-size: 40px;
            text-align: center;
            background: white;
        }

        /* Create two unequal columns that floats next to each other */
        /* Left column */
        .leftcolumn {
            float: left;
            width: 75%;
        }

        /* Right column */
        .rightcolumn {
            float: left;
            width: 25%;
            padding-left: 20px;
        }

        /* Fake image */
        .fakeimg {
            background-color: #aaa;
            width: 100%;
            padding: 20px;
        }

        /* Add a card effect for articles */
        .card {
            border: 0px;
            background-color: var(--color-bg);
            padding: 20px;
            margin-top: 20px;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/

        }/* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Footer */
        .footer {
            padding: 20px;
            text-align: center;
            background: #ddd;
            margin-top: 20px;
        }

        /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 800px) {
            .leftcolumn, .rightcolumn {
                width: 100%;
                padding: 0;
            }
        }
        .shadow-card{
            box-shadow: rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px;
        }
    </style>
    <div class="header">
        <div style="height:300px;background-image: url('https://accessnsite.com/wp-content/uploads/2020/05/page-heading-background-contact-us.jpg');box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></div>
    </div>

    <div class="container py-5" style="padding: 0px">
        <div class="row d-flex justify-content-center">
            <div class="col-md-12">
                <h3 class="heading mt-5 text-center" >Hy, Apakah Anda Ingin Mencari Karyawan ? </h3>
                <form action="" method="post" class="mb-5">
                    @csrf
                    <div class="d-flex justify-content-center px-5">
                        <div class="search"> <input type="text" class="typeahead search-input" placeholder="Search..." name="search"> <button class="search-icon"> <i class="fa fa-search"></i> </button> </div>
                    </div>
                </form>


            </div>
        </div>
        <div class="row">

            @foreach($data as $karyawan)
                <div class="col-sm-3 ">
                    <div class="card shadow-card" style="width: 18rem;">
                        <img class="card-img-top" src="{{asset($karyawan->img)}}" height="250" alt="Card image cap">
                        <div class="card-body" style="border: 20px">
                            <br>
                            <p class="card-text">{{$karyawan->name}}</p>
                            <a href="#" class="btn btn-primary">{{$karyawan->mapel}}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        </section>
    </div>

@endsection
