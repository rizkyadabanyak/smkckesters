@extends('landing.app')

@section('content')
    @include('landing.components.style-search')

    <style>
        .card-horizontal {
            display: flex !important;
            flex: 1 1 auto !important;
        }
        * {
            box-sizing: border-box;
        }

        /* Add a gray background color with some padding */
        /* Header/Blog Title */
        .header {
            font-size: 40px;
            text-align: center;
            background: white;
        }

        /* Create two unequal columns that floats next to each other */
        /* Left column */
        .leftcolumn {
            float: left;
            width: 75%;
        }

        /* Right column */
        .rightcolumn {
            float: left;
            width: 25%;
            padding-left: 20px;
        }

        /* Fake image */
        .fakeimg {
            background-color: #aaa;
            width: 100%;
            padding: 20px;
        }

        /* Add a card effect for articles */
        .card {
            border: 0px;
            background-color: var(--color-bg);
            padding: 20px;
            margin-top: 20px;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/

        }/* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Footer */
        .footer {
            padding: 20px;
            text-align: center;
            background: #ddd;
            margin-top: 20px;
        }

        /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 800px) {
            .leftcolumn, .rightcolumn {
                width: 100%;
                padding: 0;
            }
        }
        .shadow-card{
            box-shadow: rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px;
        }
        .zoom {
            -webkit-transition: all 0.35s ease-in-out;
            -moz-transition: all 0.35s ease-in-out;
            transition: all 0.35s ease-in-out;
            cursor: -webkit-zoom-in;
            cursor: -moz-zoom-in;
            cursor: zoom-in;
        }
        .zoom:hover,
        .zoom:active,
        .zoom:focus {
            /**adjust scale to desired size,
            add browser prefixes**/
            -ms-transform: scale(4);
            -moz-transform: scale(4);
            -webkit-transform: scale(4);
            -o-transform: scale(4);
            transform: scale(4);
            position:relative;
            z-index:100;
        }
    </style>
    <div class="header">
        <div style="height:300px;background-image: url('https://accessnsite.com/wp-content/uploads/2020/05/page-heading-background-contact-us.jpg');box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></div>
    </div>

    <div class="container py-5" style="max-width: 1200px;padding: 0px">

        <section class="wrapper">
            <div class="row">
                <div class="col-md-4">
                    <img src="{{asset($detail->img)}}" class="img-fluid" alt="Responsive image">

                </div>
                <div class="col-md-8">
                    <h2>{{$data->name}}</h2><br>
                    <h5>{!! $detail->dsc !!}</h5>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{asset($detail->karyawan->img)}}" class="img-fluid" alt="Responsive image">

                        </div>
                        <div class="col-md-8">
                            <h5>Nama : <br>{{$detail->karyawan->name}}</h5>
                            <h5>Bidang Keahlian : <br>{{$detail->karyawan->mapel}}</h5>
                        </div>
                    </div>
                    <b5></b5>
                </div>

            </div>
        </section>
    </div>
@endsection
