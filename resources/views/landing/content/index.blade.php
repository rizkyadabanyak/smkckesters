@extends('landing.app')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
{{--    <script src="custom.js"></script>--}}

    {{--    @include('landing.components.style-card')--}}
    @include('landing.components.style-search')
    <style>

        #demo {
            height:100%;
            position:relative;
            overflow:hidden;
        }


        .green{
            background-color:#6fb936;
        }
        .thumb{
            margin-bottom: 30px;
        }

        .page-top{
            margin-top:85px;
        }


        img.zoom {
            width: 100%;
            height: 200px;
            border-radius:5px;
            object-fit:cover;
            -webkit-transition: all .3s ease-in-out;
            -moz-transition: all .3s ease-in-out;
            -o-transition: all .3s ease-in-out;
            -ms-transition: all .3s ease-in-out;
        }


        .transition {
            -webkit-transform: scale(1.2);
            -moz-transform: scale(1.2);
            -o-transform: scale(1.2);
            transform: scale(1.2);
        }
        .modal-header {

            border-bottom: none;
        }
        .modal-title {
            color:#000;
        }
        .modal-footer{
            display:none;
        }

    </style>

    <div id="carousel" class="carousel carousel-dark slide" data-ride="carousel" data-interval="6000">
        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <a href="{{route('detail',$carousel1->slug)}}">
                    <!--
                    If you need more browser support use https://scottjehl.github.io/picturefill/
                    If a picture looks blurry on a retina device you can add a high resolution like this
                    <source srcset="img/blog-post-1000x600-2.jpg, blog-post-1000x600-2@2x.jpg 2x" media="(min-width: 768px)">

                    What image sizes should you use? This can help - https://codepen.io/JacobLett/pen/NjramL
                     -->
                    <picture>
                        <div style="background-color: black">
                            <img style="opacity: 50%" srcset="{{asset($carousel1->img)}}" alt="responsive image" class="d-block img-fluid img100">
                        </div>
                    </picture>

                    <div class="carousel-caption">
                        <div>
                            <h2 style="color: white">{{$carousel1->name}}</h2>
                            <p style="color: #c2c2c2">{!! \Str::limit($carousel1->desc_banner, 100, $end='......') !!}</p>
                            <a href="{{route('detail',$carousel1->slug)}}" class="btn btn-sm btn-outline-success">Read More</a>
                        </div>
                    </div>
                </a>
            </div>
            <!-- /.carousel-item -->

            <!-- /.carousel-item -->
            <div class="carousel-item">
                <a href="{{route('detail',$carousel2->slug)}}">

                    <picture>
                        <div style="background-color: black">
                            <img style="opacity: 50%" srcset="{{asset($carousel2->img)}}" alt="responsive image" class="d-block img-fluid img100">
                        </div>
                    </picture>

                    <div class="carousel-caption">
                        <div>
                            <h2 style="color: white">{{$carousel2->name}}</h2>
                            <p style="color: #c2c2c2">{!! \Str::limit($carousel2->desc_banner, 100, $end='......') !!}</p>
                            <a href="{{route('detail',$carousel2->slug)}}" class="btn btn-sm btn-outline-success">Read More</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="carousel-item">
                <a href="{{route('detail',$carousel3->slug)}}">
                    <picture>
                        <div style="background-color: black">
                            <img style="opacity: 50%" srcset="{{asset($carousel3->img)}}" alt="responsive image" class="d-block img-fluid img100">
                        </div>
                    </picture>

                    <div class="carousel-caption">
                        <div>
                            <h2 style="color: white">{{$carousel3->name}}</h2>
                            <p style="color: #c2c2c2">{!! \Str::limit($carousel3->desc_banner, 100, $end='......') !!}</p>
                            <a href="{{route('detail',$carousel3->slug)}}" class="btn btn-sm btn-outline-success">Read More</a>
                        </div>
                    </div>
                </a>
            </div>
            <!-- /.carousel-item -->
        </div>
        <!-- /.carousel-inner -->
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- /.carousel -->
    <div class="container py-5" style="max-width: 1200px">

        <section class="wrapper">
            <br><br>

            <div class="row">
                <div class="col-md-6" style="padding-right: 50px;">
                    <a style="border-bottom: 6px #175690 solid;font-size: 30px;" class="text-orange">Profil SMK Kesehatan</a>
                    <br><br><br>

                    <iframe style="width: 100%; height: 300px" src="{{$profile->youtube}}">
                    </iframe>
                    <p href="" class="mt-2">{{$profile->dsc}}</p>

                </div>
                <div class="col-md-6" style="border-left: 1px solid #868484;padding-left: 50px">
                    <a style="border-bottom: 6px #175690 solid;font-size: 30px;" class="text-orange">Berita Terbaru</a>
                    <br><br>
                    <div class="row">
                        @forelse($berita as $new)
                            <div class="col-12 mt-3">
                                <div class="card" style="border: 0px">
                                    <div class="card-horizontal">
                                        <div class="img-square-wrapper">
                                            <img class="img-card" src="{{asset($new->img)}}" style="height: 150px " alt="Card image cap">
                                        </div>
                                        <div class="card-body" style="padding-top: 0px !important;">
                                            <a href="{{route('detail',$new->slug)}}" style="color: black">
                                                <p class="card-title" style="font-family: 'Segoe UI';font-size: 20px">{{\Str::limit($new->name, 70, $end='...') }}</p>
                                            </a>
                                            <p class="card-text" style="font-family: 'Segoe UI';color: #175690">{{$new->kategori->name}}</p>
                                            <p class="card-text" style="font-family: 'Segoe UI';color: var(--color-scnd);">{!! \Str::limit($new->desc_banner, 100, $end='......') !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{route('detail',$new->slug)}}" style="text-decoration: none;color: #175690;font-size: 15px">Read More</a>
                                <hr size="10px" style="color: rgba(0,0,0,0.45);border: 1px solid" />
                            </div>
                        @empty
                            <h5 class="mt-5">belum ada berita</h5>
                        @endforelse

                    </div>
                    <div class="d-flex justify-content-between">
                        <div>
                        </div>
                        <div>
                            <a class="btn btn-success btn-sm mt-3 text-right"  href="{{route('moreNew')}}"> Selengkapnya >></a>

                        </div>
                    </div>
                </div>
            </div>
            <br><br>


{{--            <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">Gallery</a>--}}
{{--            <p href="" class="mt-2">Menyajikan informasi dari Pemerintahan provinsi jawa timur yang disajikan dalam bentuk foto dan Vidio</p>--}}


{{--            <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">WEBSITE TERKAIT</a>--}}
{{--            <p href="" class="mt-2">Menyajikan Website Terkait dari DLH Jatimprov</p>--}}
            <center>

            </center>
        </section>
    </div>

    <div style="background-color: #175690">
        <div class="container">
            <div class="row">
                <div class="col-md-8" style="padding-top: 40px;padding-bottom: 40px">
                    <h2 style="color: white;margin-bottom: 10px">Prakata Kepala Sekolah</h2>
                    <p style="color:white;text-align: justify;font-size: 15px">{{$kepsek->dsc}}</p>
                </div>
                <div class="col-md-4">
                    <img class="img-card" src="{{asset($kepsek->img)}}" style="width: 200px" alt="Card image cap">

                </div>
            </div>
        </div>
    </div>



@endsection
