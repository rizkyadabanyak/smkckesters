<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<style>
    :root {
        --color-font: #424242;
        --color-bg: #fff;
        --color-head: #292922;
        --color-pmry: #302AE6;
        --color-scnd: #536390;
        --color-inner: #161625;

    }
    [data-theme="dark"] {
        --color-pmry: #9A97F3;
        --color-scnd: #818cab;
        --color-font: #e1e1ff;
        --color-bg: #161625;
        --color-head: #818cab;
        --color-inner: #161625;

    }
    .text-orange{
        color: #ff9200;
    }
    p{
        color: var(--color-font);
    }
    .card-horizontal {
        display: flex;
        flex: 1 1 auto;
    }
    .card{
        background-color: var(--color-bg);
    }
    .img-card{
        width: 200px;
        height: 100%;
    }
    @media (max-width: 575.98px) {
        .padding-a{
            padding-right: 10px!important; padding-left: 10px!important;
        }
        .card-horizontal {
            display: block;
            flex: 1 1 auto;
        }
        .img-card{
            width: 100%;
        }
    }

    @media (max-width: 767.98px) { .padding-a{ padding-right: 10px!important; padding-left: 10px!important;}  }
    /*body {background:#e2e2e2;}*/
    @font-face {
        font-family: Poppins-Medium;
        src: url({{asset('font/Poppins-Medium.ttf')}});
    }

    * {
        font-family: Poppins-Medium;
    }

    @media (min-width: 992px) { .display-none{
        display: none;
    } }

    @media (min-width: 1200px) { .display-none{
        display: none;
    }
    .img100{
        height: 100vh;
        width: 1920px;
    }
    }

    .bg-active-nav{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

        background: #175690;
        /*background-image: linear-gradient(to bottom right,#00b712, #5aff15);*/
    }



    body {
        background-color: var(--color-bg);
        color: var(--color-font);
    }

    em {
        margin-top: 0.5em;
        margin-left: 1em;
        font-size: 1rem;
    }
    .theme-switch {
        display: inline-block;
        height: 34px;
        position: relative;
        width: 60px;
    }


    .theme-switch input {
        display:none;
    }
    .slider {
        background-color: #ccc;
        bottom: 0;
        cursor: pointer;
        left: 0;
        position: absolute;
        right: 0;
        top: 0;
        transition: .4s;
    }

    .slider:before {
        background-color: #fff;
        bottom: 4px;
        content: "";
        height: 26px;
        left: 4px;
        position: absolute;
        transition: .4s;
        width: 26px;
    }

    input:checked + .slider {
        background-color: #e2b030;
    }




    input:checked + .slider:before {
        transform: translateX(26px);
    }
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }


</style>
