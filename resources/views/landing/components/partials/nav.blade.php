<nav class="navbar navbar-expand-md fixed-top navbar-dark padding-a" style="padding-right: 100px;padding-left: 100px;padding-top: 10px;padding-bottom: 10px">
    <a class="navbar-brand" href="#">
        <img src="{{asset('assets/logo-fix.png')}}" width="230" height="70" alt="">
    </a>
    <button style="background: #4898e1" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon "></span>
    </button>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active" style="margin-right: 10px">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown active" style="margin-right: 10px">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Profil
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{route('visimisi')}}" style="font-weight: lighter">Visi & Misi</a>
                    <a class="dropdown-item" href="{{route('struktur')}}">Struktur Organisasi</a>
                    <a class="dropdown-item" href="{{route('karyawan')}}">Guru & Tenaga Pendidikan</a>
                    <a class="dropdown-item" href="{{route('sarana')}}">Sarana & Prasarana</a>
                    <a class="dropdown-item" href="{{route('galery')}}">Gallery Foto</a>
{{--                    <a class="dropdown-item" href="{{route('regulations')}}">PERATURAN/UU</a>--}}
                </div>
            </li>
            <li class="nav-item dropdown active" style="margin-right: 10px">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Jurusan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @foreach($jurusans as $jurusan)
                    <a class="dropdown-item" href="{{route('detailJurusan',$jurusan->id)}}">{{$jurusan->name}}</a>
                    @endforeach
                    {{--                    <a class="dropdown-item" href="{{route('regulations')}}">PERATURAN/UU</a>--}}
                </div>
            </li>

            <li class="nav-item dropdown active" style="margin-right: 10px">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Informasi
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{route('pengumuman')}}" style="font-weight: lighter">Pengumuman</a>
                    {{--                    <a class="dropdown-item" href="{{route('regulations')}}">PERATURAN/UU</a>--}}
                </div>
            </li>

            <li class="nav-item active" style="margin-right: 10px">
                <a class="nav-link" href="{{route('moreNew')}}">Berita <span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item active" style="margin-right: 10px">
                <a class="nav-link" href="{{route('kontak')}}">Kontak <span class="sr-only">(current)</span></a>
            </li>

{{--            <li class="nav-item">--}}
{{--                <label class="theme-switch" for="checkbox">--}}
{{--                    <input type="checkbox" id="checkbox"/>--}}
{{--                    <div class="slider round"></div>--}}
{{--                </label>--}}
{{--            </li>--}}

        </ul>
    </div>

</nav>

