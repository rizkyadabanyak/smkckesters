<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $jurusans = \App\Models\Jurusan::all();
    $berita = \App\Models\Berita::whereStatus('active')->orderBy('created_at','DESC')->limit(3)->get();
    $prfile = \App\Models\Profile::first();
    $kepsek = \App\Models\Kepsek::first();
    $carousel = [];
    $i = 0;

    foreach ($berita as $a){
        $carousel[$i] = $a;
        $i++;
    }
    $carousel1 = $carousel[0];
    $carousel2 = $carousel[1];
    $carousel3 = $carousel[2];

//    dd($carousel);

    view()->share([
        'jurusans' => $jurusans,
        'berita' => $berita,
        'profile' => $prfile,
        'kepsek' => $kepsek,
        'carousel1' => $carousel1,
        'carousel2' => $carousel2,
        'carousel3' => $carousel3
    ]);
    return view('landing.content.index');
});

Route::group(['prefix'=>'admin','as'=>'admin.','middleware'=>['auth']],function (){

    Route::group(['as'=>'auth.'],function (){
        Route::get('dashboard',[\App\Http\Controllers\Admin\DashboardController::class,'index'])->name('dashboard');

        Route::resource('jurusan', \App\Http\Controllers\Admin\JurusanController::class);
        Route::get('jurusan/destroy/{id}',[\App\Http\Controllers\Admin\JurusanController::class,'destroy'])->name('jurusanDestroy');
        Route::get('jurusan/detail/{id}',[\App\Http\Controllers\Admin\JurusanController::class,'detailForm'])->name('jurusanDetail');
        Route::post('jurusan/detail/{id}',[\App\Http\Controllers\Admin\JurusanController::class,'detailFormAction'])->name('jurusanDetailAction');
//        Route::pu('jurusan/detail/{id}',[\App\Http\Controllers\Admin\JurusanController::class,'detailFormActionEdit'])->name('jurusanDetailActionEdit');

        Route::resource('VisiMisi', \App\Http\Controllers\Admin\VisiMisiController::class);
        Route::get('VisiMisi/destroy/{id}',[\App\Http\Controllers\Admin\VisiMisiController::class,'destroy'])->name('VisiMisiDestroy');

        Route::resource('Struktur', \App\Http\Controllers\Admin\StrukturController::class);
        Route::get('Struktur/destroy/{id}',[\App\Http\Controllers\Admin\StrukturController::class,'destroy'])->name('strukturDestroy');

        Route::resource('karyawan', \App\Http\Controllers\Admin\KaryawanController::class);
        Route::get('karyawan/destroy/{id}',[\App\Http\Controllers\Admin\KaryawanController::class,'destroy'])->name('karyawanDestroy');

        Route::resource('berita', \App\Http\Controllers\Admin\BeritaController::class);
        Route::get('berita/destroy/{id}',[\App\Http\Controllers\Admin\BeritaController::class,'destroy'])->name('beritaDestroy');

        Route::resource('kategori', \App\Http\Controllers\Admin\KategoriController::class);
        Route::get('kategori/destroy/{id}',[\App\Http\Controllers\Admin\KategoriController::class,'destroy'])->name('kategoriDestroy');

        Route::resource('profile', \App\Http\Controllers\Admin\ProfileController::class);
        Route::get('profile/destroy/{id}',[\App\Http\Controllers\Admin\ProfileController::class,'destroy'])->name('kategoriDestroy');

        Route::resource('kepsek', \App\Http\Controllers\Admin\KepsekController::class);
        Route::get('kepsek/destroy/{id}',[\App\Http\Controllers\Admin\KepsekController::class,'destroy'])->name('kepsekDestroy');

        Route::resource('sarana', \App\Http\Controllers\Admin\SaranaController::class);
        Route::get('sarana/destroy/{id}',[\App\Http\Controllers\Admin\SaranaController::class,'destroy'])->name('saranaDestroy');

        Route::resource('galery', \App\Http\Controllers\Admin\GaleryController::class);
        Route::get('galery/destroy/{id}',[\App\Http\Controllers\Admin\GaleryController::class,'destroy'])->name('galeryDestroy');

        Route::resource('pengumuman', \App\Http\Controllers\Admin\PengumumanController::class);
        Route::get('pengumuman/destroy/{id}',[\App\Http\Controllers\Admin\PengumumanController::class,'destroy'])->name('pengumumanDestroy');

    });
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/visimisi', [App\Http\Controllers\Landing\VisiMisiController::class, 'index'])->name('visimisi');
Route::get('/struktur', [App\Http\Controllers\Landing\StrukturController::class, 'index'])->name('struktur');
Route::get('/pengumuman', [App\Http\Controllers\Landing\StrukturController::class, 'pengumuman'])->name('pengumuman');
Route::get('/karyawan', [App\Http\Controllers\Landing\KaryawanController::class, 'index'])->name('karyawan');
Route::get('/saranaPrasarana', [App\Http\Controllers\Landing\SaranaController::class, 'index'])->name('sarana');
Route::get('/galery', [App\Http\Controllers\Landing\GaleryController::class, 'index'])->name('galery');
Route::get('/detail/{slug}',[\App\Http\Controllers\HomeController::class,'detail'])->name('detail');
Route::get('/detailJurusan/{id}',[\App\Http\Controllers\HomeController::class,'detailJurusan'])->name('detailJurusan');
Route::get('more-new',[\App\Http\Controllers\HomeController::class,'moreNew'])->name('moreNew');
Route::get('/kontak',[\App\Http\Controllers\HomeController::class,'kontak'])->name('kontak');
Route::post('search',[\App\Http\Controllers\HomeController::class,'searchAction'])->name('searchAction');
