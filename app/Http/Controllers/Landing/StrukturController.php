<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Models\Pengumuman;
use App\Models\Struktur;
use App\Models\VisiMisi;
use Illuminate\Http\Request;

class StrukturController extends Controller
{
    public function index(){
//        dd('dwqdqw');
        $data = Struktur::first();
        $jurusans = \App\Models\Jurusan::all();

        view()->share([
            'data' => $data,
            'jurusans' => $jurusans
        ]);
        return view('landing.content.struktur');
    }
    public function pengumuman(){
//        dd('dwqdqw');
//        $data = Struktur::first();
        $jurusans = \App\Models\Jurusan::all();
        $pengumuman = Pengumuman::all();
        view()->share([
//            'data' => $data,
        'pengumuman' => $pengumuman,
            'jurusans' => $jurusans
        ]);
        return view('landing.content.pengumuman');
    }
}
