<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Models\Galery;
use App\Models\Sarana;
use Illuminate\Http\Request;

class GaleryController extends Controller
{
    public function index(){
        $data = Galery::all();
        $jurusans = \App\Models\Jurusan::all();

        view()->share([
            'data' => $data,
            'jurusans' => $jurusans
        ]);
        return view('landing.content.galery');
    }
}
