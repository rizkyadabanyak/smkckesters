<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Models\Karyawan;
use App\Models\Sarana;
use Illuminate\Http\Request;

class SaranaController extends Controller
{
    public function index (){
        $data = Sarana::all();
        $jurusans = \App\Models\Jurusan::all();

        view()->share([
            'data' => $data,
            'jurusans' => $jurusans
        ]);
        return view('landing.content.sarana');
    }
}
