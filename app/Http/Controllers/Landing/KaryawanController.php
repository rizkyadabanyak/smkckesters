<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Models\Karyawan;
use App\Models\Struktur;
use Illuminate\Http\Request;

class KaryawanController extends Controller
{
    public function index(){
//        dd('dwqdqw');
        $data = Karyawan::all();
        $jurusans = \App\Models\Jurusan::all();

        view()->share([
            'data' => $data,
            'jurusans' => $jurusans
        ]);
        return view('landing.content.karyawan');
    }
}
