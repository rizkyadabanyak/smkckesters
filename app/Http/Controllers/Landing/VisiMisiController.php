<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Models\VisiMisi;
use Illuminate\Http\Request;

class VisiMisiController extends Controller
{
    public function index(){
//        dd('dwqdqw');
        $data = VisiMisi::first();
        $jurusans = \App\Models\Jurusan::all();

        view()->share([
            'data' => $data,
            'jurusans' => $jurusans
        ]);
        return view('landing.content.visimisi');
    }
}
