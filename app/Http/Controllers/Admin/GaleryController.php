<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Galery;
use App\Models\Galleries;
use App\Models\Sarana;
use Illuminate\Http\Request;

class GaleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Galery::all();

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.galery.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.galery.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            $files = $request->file('image');
//            dd($files);
            foreach ($files as $file){

                $data = new Galery();
                $data->name = $request->name;

                $name = rand(999999999,1);
                $extension = $file->getClientOriginalExtension();
                $newName = $name.'.'.$extension;

                $imgDB = 'uploads/newsGalleries/'.$newName;
                $file->move(public_path('uploads/newsGalleries/'), $newName);

                $data->img = $imgDB;

                $data->save();
            }
        }

        $data->save();
        return redirect()->route('admin.auth.galery.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Galery::find($id);

        view()->share([
            'data' => $data
        ]);

        return view('admin.content.galery.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Galery::find($id);

        $data->name = $request->name;

        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/'.$newName;

            $request->image->move(public_path('uploads/'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $data->img ;
        }

        $data->save();
        return redirect()->route('admin.auth.galery.index')->withSuccess('Succcess edit data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Galery::find($id);

//        dd($data);
        $data->delete();

        return redirect()->route('admin.auth.galery.index')->withSuccess('Succcess Delete data');
    }
}
