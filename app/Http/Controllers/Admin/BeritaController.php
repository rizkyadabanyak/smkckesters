<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Berita;
use App\Models\Category;
use App\Models\Img_berita;
use App\Models\Karyawan;
use App\Models\Kategori;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Berita::whereStatus('active')->get();

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.berita.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Kategori::all();
        $data = null;

        view()->share([
            'categories' => $categories,
            'data' => $data
        ]);
        return view('admin.content.berita.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validates = [
            'category_id' => 'required',
            'name' => 'required|unique:beritas,name',
            'desc' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,svg'
        ];

        $request->validate($validates);

        $data = new Berita();

        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/news/'.$request->kategori_id.'/'.$newName;

            $request->image->move(public_path('uploads/news/'.$request->kategori_id.'/'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $data->img ;
        }

        $data->user_id = Auth::user()->id;
        $data->kategori_id = $request->category_id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->desc = $request->desc;
        $data->desc_banner = strip_tags($request->desc);
        $data->save();

//        dd($data->id);


        return redirect()->route('admin.auth.berita.index')->withSuccess('Succcess create data');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Kategori::all();
        $data = Berita::find($id);

        view()->share([
            'categories' => $categories,
            'data' => $data
        ]);
        return view('admin.content.berita.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validates = [
            'category_id' => 'required',
//            'name' => 'required|unique:beritas,name',
            'desc' => 'required',
//            'image' => 'required|mimes:jpeg,png,jpg,svg'
        ];

        $request->validate($validates);

        $data = Berita::find($id);

        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/news/'.$request->kategori_id.'/'.$newName;

            $request->image->move(public_path('uploads/news/'.$request->kategori_id.'/'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $data->img ;
        }

        $data->user_id = Auth::user()->id;
        $data->kategori_id = $request->category_id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->desc = $request->desc;
        $data->desc_banner = strip_tags($request->desc);
        $data->save();

//        dd($data->id);


        return redirect()->route('admin.auth.berita.index')->withSuccess('Succcess create data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Berita::find($id);

        $data->status = 'non-active';
        $data->save();

        return redirect()->route('admin.auth.berita.index')->withDanger('Succcess non-active data');;

    }
}
