<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Jurusan;
use App\Models\VisiMisi;
use Illuminate\Http\Request;

class VisiMisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = VisiMisi::first();

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.visimisi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.visimisi.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new VisiMisi();

        $data->visi = $request->visi;
        $data->misis = $request->misi;

        $data->save();
        return redirect()->route('admin.auth.VisiMisi.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = VisiMisi::find($id);
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.visimisi.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = VisiMisi::find($id);

        $data->visi = $request->visi;
        $data->misi = $request->misi;

        $data->save();
        return redirect()->route('admin.auth.VisiMisi.index')->withSuccess('Succcess Edit data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
