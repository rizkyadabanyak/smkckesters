<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DetailJurusan;
use App\Models\Jurusan;
use App\Models\Karyawan;
use App\Models\Level;
use Illuminate\Http\Request;

class JurusanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Jurusan::all();

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.jurusan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.jurusan.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Jurusan();

        $data->name = $request->name;

        $data->save();
        return redirect()->route('admin.auth.jurusan.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Jurusan::find($id);
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.jurusan.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Jurusan::find($id);

        $data->name = $request->name;

        $data->save();
        return redirect()->route('admin.auth.jurusan.index')->withSuccess('Succcess Edit data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Jurusan::find($id);

//        dd($data);
        $data->delete();

        return redirect()->route('admin.auth.jurusan.index')->withSuccess('Succcess Edit data');

    }
    public function detailForm($id){

        $data = Jurusan::find($id);
        $detail = DetailJurusan::whereJurusanId($data->id)->first();

//        dd($detail!=null);
//        if ($detail!=null){
//            $data =
//        }

        $karyawan = Karyawan::all();
        view()->share([
            'data' => $data,
            'karyawan'=>$karyawan,
            'detail' => $detail
        ]);
        return view('admin.content.jurusan.formDetail');
    }

    public function detailFormAction($id,Request $request){

        $data = Jurusan::find($id);
        $detaila = DetailJurusan::whereJurusanId($data->id)->first();

        if ($detaila != null){
            $detail = DetailJurusan::whereJurusanId($data->id)->first();
        }else{
            $detail = new DetailJurusan();
        }

        if($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/news/'.$request->kategori_id.'/'.$newName;

            $request->image->move(public_path('uploads/news/'.$request->kategori_id.'/'), $newName);
            $detail->img = $imgDB;
        }else{

            $detail->img = $detail->img;
        }

        $detail->jurusan_id  = $data->id;
        $detail->kaprodi = $request->kaprodi;
        $detail->dsc = $request->desc;

        $detail->save();



        return redirect()->route('admin.auth.jurusan.index')->withSuccess('Succcess Edit data');
    }


}
