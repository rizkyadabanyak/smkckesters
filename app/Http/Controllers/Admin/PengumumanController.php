<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pengumuman;
use App\Models\Sarana;
use Illuminate\Http\Request;

class PengumumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pengumuman::all();

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.pengumuman.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.pengumuman.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Pengumuman();

        $data->name = $request->name;

        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/'.$newName;

            $request->image->move(public_path('uploads/'), $newName);
            $data->file = $imgDB;
        }else{
            $data->file = $data->image ;
        }

        $data->save();
        return redirect()->route('admin.auth.pengumuman.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Pengumuman::find($id);
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.pengumuman.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Pengumuman::find($id);

        $data->name = $request->name;

        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/'.$newName;

            $request->image->move(public_path('uploads/'), $newName);
            $data->file = $imgDB;
        }else{
            $data->file = $data->file ;
        }

        $data->save();
        return redirect()->route('admin.auth.pengumuman.index')->withSuccess('Succcess edit data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pengumuman::find($id);

//        dd($data);
        $data->delete();

        return redirect()->route('admin.auth.pengumuman.index')->withSuccess('Succcess Delete data');
    }
}
