<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use App\Models\Berita;
use App\Models\CommentNews;
use App\Models\DetailJurusan;
use App\Models\Img_berita;
use App\Models\Jurusan;
use App\Models\News;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function detail ($slug){

        $cek = 'new';
        $data = Berita::whereSlug($slug)->first();
        $news = Berita::whereStatus('active')->orderBy('created_at', 'DESC')->limit(3)->get();
        $jurusans = Jurusan::all();

        view()->share([
            'data' => $data,
            'news' =>$news,
            'cek' => $cek,
            'jurusans' => $jurusans
        ]);

        return view('landing.content.detail');
    }
    public function detailJurusan ($id){

//        $cek = 'new';
        $data = Jurusan::find($id);
        $jurusans = Jurusan::all();
        $detail = DetailJurusan::whereJurusanId($data->id)->first();

        view()->share([
            'data' => $data,
//            'cek' => $cek,
            'jurusans' => $jurusans,
            'detail' => $detail
        ]);

        return view('landing.content.detailJurusan');
    }

    public function moreNew(Request $request){
//        dd($request->search);

        $data = Berita::whereStatus('active')->orderBy('created_at','DESC')->paginate(5);
        $jurusans = Jurusan::all();

        view()->share([
            'news' => $data,
            'jurusans' => $jurusans


        ]);

        return view('landing.content.moreNew');
    }
    public function kontak(Request $request){
//        dd($request->search);
        $jurusans = Jurusan::all();

        view()->share([
            'jurusans' => $jurusans
        ]);

        return view('landing.content.kontak');
    }
    public function searchAction(Request $request){
//        dd($request->search);
        $data = Berita::where('name','like',"%".$request->search."%")->get();
        $jurusans = Jurusan::all();

        view()->share([
            'news' => $data,
            'jurusans' => $jurusans

        ]);

        return view('landing.content.search');
    }
}
